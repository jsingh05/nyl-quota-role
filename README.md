NYL-Quota-Role
=========

This role utilizes the NetApp API to create a quota. 
The latest documentaion on the Ansible/NetApp API used by this role can be found at: 
https://docs.ansible.com/ansible/latest/collections/netapp/ontap/na_ontap_quotas_module.html

Requirements
------------
This role requires:

1. Python, Version 3.6.9
2. netapp.ontap, Version 21.10.0
3. netapp-lib, Version 2021.6.25

Role Variables
--------------
(Required)

*netapp_hostname:*

   - This variable should be set to the NetApp hostname that is used to manage the export policy
   
*netapp_username:*

   - This variable should be set to the NetApp username that is used to manage the export policy
   
*netapp_password:*

   - This variable should be set to the NetApp password that is used to manage the export policy

*svm:*

   - This variable will set the VServer that will be used to manage the export policy

*quota_type:*
   
   - The type of quota rule (Choices: user, group, and tree)

*quota_target:*
   
   - The quota target of the type specified. Required to create or modify a rule.

*qtree_name:*

   - Name of the qtree for the quota. For user or group rules, it can be the qtree name or "" if no qtree. For tree type rules, this field must be "".

(Optional)

*protocol_http_port:*

   - Override the default port (80 or 443) with this port
   
*protocol_ontapi:*

   - The ontap api version to use

*protocol_use_rest:*

   - REST API if supported by the target system for all the resources and attributes the module requires. Otherwise will revert to ZAPI.

*protocol_validate_certs:*
   
   - If set to no, the SSL certificates will not be validated. This should only set to False used on personally controlled sites using self-signed certificates.

*protocol_cert_filepath:*
   
   - path to SSL client cert file (.pem).

*protocol_key_filepath:*
   
   - path to SSL client key file.
  
*quota_disk_limit:*
   
   - List of Read Write access specifications for the rule

*quota_file_limit:*
   
   - User name or ID to which anonymous users are mapped. Default value is '65534'.

*quota_status:*

   - Whether the specified volume should have quota status on or off.
   
*protocol_security_style:*

   - The security style for the qtree. (unix, ntfs, or mixed)

*quota_threshold:*

   - The amount of disk space the target would have to exceed before a message is logged.

*quota_activate_on_change:*

   - Method to use to activate quota on a change.


Example Playbook
----------------

---
- name: Create quota
  hosts:
    - all
  gather_facts: false
  become: false
  vars_files:
    - vars/ontap_creds.yml
    - vars/main.yml
  include_roles:
    - nyl-quota-role



Author Information
------------------

This role was developed by Enterprise Vision Technologies (*www.evtcorp.com*)

